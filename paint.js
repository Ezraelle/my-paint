window.addEventListener("load", () => {
	
	var canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");
	var rubber = $('#rubber');
	var crayon = $('#pen');
	let painting = false;
	var color = "pink";
	var size = 0;

	function startPosition(e)
	{
		painting = true;
		paint(e);
	}

	function endPosition()
	{
		painting = false;
		ctx.beginPath();
	}

	function paint(e)
	{
		if(!painting) return;
		ctx.lineWidth = size;
		ctx.lineCap = "round";
		ctx.strokeStyle = color;

		ctx.lineTo(e.clientX, e.clientY);
		ctx.stroke();
		ctx.beginPath();
		ctx.moveTo(e.clientX,e.clientY);
	}

	function gomme()
	{
		color = "white";
	}

	function pen()
	{
		color = "black";
	}

	canvas.addEventListener('mousedown', startPosition);
	canvas.addEventListener('mouseup', endPosition);
	canvas.addEventListener('mousemove', paint);


	$('#color').change(function()
	{
		color = $(this).val();
	});

	$('#size').change(function(){

		size = $(this).val();
	});

	$('#rubber').click(function(){
		gomme();
	});

	$('#pen').click(function(){
		pen();
	});

	$('#line').click(function(){
		$("#canvas").mousedown(function(e)
		{
			ctx.beginPath();
			ctx.moveTo(e.clientX, e.clientY);
		});
		$("#canvas").mouseup(function(e)
		{
			ctx.lineTo(e.clientY, e.clientX);
			ctx.stroke();
		});
	});

	$("#circle").click(function(){
		$("canvas").mousedown(function(){
			ctx.beginPath();
		});

		$("canvas").mousemove(function(){
			ctx.arc(5, 75, 50, 0, 2 * Math.PI);
		});

		$("canvas").mouseup(function(){
			ctx.stroke();
		});
	});
});


